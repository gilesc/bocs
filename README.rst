Tools for BOCS analysis.

Variable names
==============

A few variable names are used throughout the code:

X : :class:`pandas.DataFrame`
    A matrix with loci as rows and a MultiIndex as columns with two levels: "n" and "N".
    "n" is number of methylated loci and "N" is read depth.

D : :class:`pandas.DataFrame`
    A design matrix, used for linear modeling.

P : :class:`pandas.DataFrame`
    A matrix of loci X samples. Indicates the proportion of 
    methylated reads (in range 0-1).

Pn : :class:`pandas.DataFrame`
    P after VSN.
