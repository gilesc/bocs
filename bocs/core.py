import functools

import pandas as pd
import patsy
import numpy as np
import statsmodels.api as sm

import bocs.util

@functools.lru_cache()
def _parse(bed_path, design_path):
    X = pd.read_csv(bed_path, sep="\t")
    # FIXME: perhaps index should be a MultiIndex with all locus data separate
    X.index = ["{}:{}-{}:{}".format(*row) for row in X.iloc[:,:4].to_records(index=False)]
    X = X.iloc[:,4:]

    columns = [c.split("_") for c in X.columns]
    X.columns = pd.MultiIndex.from_tuples(columns, names=["SampleID", "Data"])
    N = X.xs("cov", level=1, axis=1)
    n = (N * X.xs("meth", level=1, axis=1) / 100).astype(int)
    X = pd.concat([n,N], keys=["n","N"], axis=1)

    D = pd.read_csv(design_path, sep="\t").set_index(["SampleID"])
    groups = D["Group"]
    D = D.drop("Group", axis=1)
    return X, D, groups

def vst(P, alpha=0.1):
    """
    Perform variance stabilizing transformation (VST) on P.

    Arguments
    =========
    alpha : float, range (0,1)
        The amount of shrinkage to apply. A higher number indicates more shrinkage,
        and will reduce the effects of P near 0 or 1 more.
    """
    P_shrink = (P * (1 - alpha * 2)) + alpha
    Pn = (P_shrink / (1 - P_shrink)).apply(np.log)
    return Pn.replace([np.inf, -np.inf], np.nan)\
            .dropna(how="any", axis=1)

class Experiment(object):
    """
    Object representing a BOCS experiment, including the methylation data and
    design matrix / group metadata.
    """
    def __init__(self, X, D, groups):
        self.X = X
        self.D = D
        self.groups = groups

    def subset(self, min_coverage):
        Xs = bocs.util.subset(self.X, min_coverage=min_coverage)
        return Experiment(Xs, self.D, self.groups)

    @property
    def P(self):
        return self.X.n / self.X.N

    @property
    def Pn(self):
        return vst(self.P)

    def mean_variance(self):
        return self.Pn.groupby(self.groups, axis=1).var().mean()

    def differential_variance(self, reference, method="OLS"):
        """
        Fit a regression model to find differences in methylation variance
        per group.

        Arguments
        =========
        Pn : :class:`pandas.DataFrame`
            P after VST.
        reference : str
            The reference group for the regression. Coefficients and p-values
            will be interpreted with reference to this group (i.e., if the reference
            is 'YM", the p-value for 'YF' will be the p-value for YM v YF).
        method : str in ('OLS')
            The modeling approach to use.

        Returns
        =======
        A statsmodels model for the fit. Use 'model.summary()' to display important
        parameters.

        Notes
        =====
        """
        # method 1: calculate variance per group and regress
        # method 2: transform p with pv=abs(p - mean(group)) and y=pv - mean(pv)

        assert method in ("OLS")

        Pn = self.Pn
        groups = self.groups

        #y = (Pn - Pn.groupby(groups, axis=1).median()).abs()

        if method == "OLS":
            y = Pn.groupby(groups, axis=1).var()
            Di = pd.Series(np.tile(y.columns, y.shape[0]))
            Di.name = "Group"
            formula = "C(Group, Treatment('{}'))".format(reference)
            D = patsy.dmatrix(formula, data=Di.to_frame(), return_type="dataframe")
            y = np.array(np.array(y).flat)
            model = sm.OLS(endog=y, exog=D)
            return model.fit()

        #model = sm.GLM(endog=y, exog=D, family=sm.families.Gamma())

    @staticmethod
    def parse(bed_path, design_path):
        return Experiment(*_parse(bed_path, design_path))
