"""
Utilities for plotting parameters of BOCS data.
"""

import matplotlib.pyplot as plt
import seaborn as sns

from bocs.util import sample

def _joint_density(x, y, scale=0.25):
    f, ax = plt.subplots(figsize=(6, 6))
    xr = x.max() - x.min()
    yr = y.max() - y.min()
    plt.xlim((x.min() - xr * scale, x.max() + xr * scale))
    plt.ylim((y.min() - yr * scale, y.max() + yr * scale))

    sns.kdeplot(x, y, ax=ax)
    sns.rugplot(x, color="g", ax=ax)
    sns.rugplot(y, vertical=True, ax=ax)
    return ax

def joint_density(P, gx, gy, fast=True):
    """
    Plot the joint density between two groups.

    X : :class:`pandas.DataFrame`
        Potentially subsetted matrix of loci vs samples.
    gx, gy : str
        Group names
    """
    p = X.n / X.N
    group = [x[:-1] for x in p.columns]
    pg = p.groupby(group, axis=1).mean()
    if fast is True:
        pg = sample(pg)
    return _joint_density(pg[gx], pg[gy])

def _vmplot(P, x_fn, y_fn, sample_size):
    plt.clf()
    P = sample(P, sample_size)
    groups = [x[:-1] for x in P.columns]

    f, ax = plt.subplots(figsize=(6,6))

    x = x_fn(P)
    y = y_fn(P)
    x = np.array(np.array(x).flat)
    y = np.array(np.array(y).flat)
    sns.regplot(x,y,fit_reg=False,ax=ax)
    sns.rugplot(x, color="g", ax=ax)
    sns.rugplot(y, vertical=True, ax=ax)
    return ax

def median_vs_mad(P, title=None):
    ax = _vmplot(P, lambda P: P.median(axis=1), lambda P: P.mad(axis=1))
    plt.xlabel("Median(p)")
    plt.ylabel("MAD(p)")
    if title is not None:
        plt.title(title)
    return ax

def variance_vs_mean(P, title=None):
    ax = _vmplot(P, lambda P: P.mean(axis=1), lambda P: P.var(axis=1))
    plt.xlabel("Mean(p)")
    plt.ylabel("Variance(p)")
    if title is not None:
        plt.title(title)
    return ax
