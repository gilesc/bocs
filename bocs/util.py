"""
Utility functions.
"""

def sample(X, n=25000):
    """
    Select random rows from input matrix. 

    Arguments
    =========
    X : :class:`pandas.DataFrame`
        The input matrix (can be any matrix, not just "X" per se).
    n : int
        Number of rows to select.
    """
    return X.loc[np.random.choice(X.index, size=n, replace=False),:]

def subset(X, min_coverage=None, p_range=(0,1)):
    """
    Subset X by query parameters.

    Arguments
    =========
    X : :class:`pandas.DataFrame`
        The input matrix.
    min_coverage : int, optional
        Select only loci with all samples min coverage >= this value.
    p_range : 2-tuple of int
        Select only loci with all samples P in this range (can be used
        to run tests removing loci near 0 or 1).
    """
    if min_coverage is not None:
        ix = (X.N >= min_coverage).all(axis=1)
        X = X.ix[ix,:]

    p = X.n / X.N
    ix = ((p >= p_range[0]) & (p <= p_range[1])).all(axis=1)
    X = X.ix[ix,:]
    return X


